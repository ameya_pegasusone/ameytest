//
//  main.m
//  Animation
//
//  Created by Pegasus Mac Mini on 19/05/18.
//  Copyright © 2018 Pegasus Mac Mini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
