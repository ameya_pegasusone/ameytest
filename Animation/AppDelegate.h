//
//  AppDelegate.h
//  Animation
//
//  Created by Pegasus Mac Mini on 19/05/18.
//  Copyright © 2018 Pegasus Mac Mini. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

